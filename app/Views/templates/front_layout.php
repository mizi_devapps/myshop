<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Belajar Bootstrap</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<!-- Nav Bar -->   
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<a class="navbar-brand" href="/">
      <img src="/img/logo-kp.png" width="30" height="30" class="d-inline-block align-top" alt="">
      KelasProgramming.com
    </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      
    </ul>
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="/bakul">Cart</a>
      </li>
    </ul>   
  </div>
</nav>
<!-- End Nav Bar -->
    <?= $this->renderSection('hero') ?>
 
<div class="container mt-5">
    <?= $this->renderSection('main-content') ?>    
</div><!-- end container -->

<footer class="text-center p-5 mt-5">Hakcipta terpelihara &copy 2021</footer>
</body>
</html>