<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		/* $all_pekan = [
		
			[
				'nama' => 'Seremban',
				'gambar' => 'https://images.unsplash.com/photo-1570036340722-b301922be4b5?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8bmVnZXJpJTIwc2VtYmlsYW58ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
			 ],

			[
				'nama' => 'Gemas',
				'gambar' => 'https://images.unsplash.com/photo-1612696964842-af32a6788bd1?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8bmVnZXJpJTIwc2VtYmlsYW58ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
			],

			[
				'nama' => 'Kuala Pilah',
				'gambar' => 'https://images.unsplash.com/photo-1615870795127-1c13f1d92997?ixid=MXwxMjA3fDB8MHxzZWFyY2h8OXx8bmVnZXJpJTIwc2VtYmlsYW58ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
			],

			[
				'nama' => 'Juaseh',
				'gambar' => 'https://images.unsplash.com/photo-1578928154540-a5bd2707133e?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTR8fG5lZ2VyaSUyMHNlbWJpbGFufGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
			],

			[
				'nama' => 'Rembau',
				'gambar' => 'https://images.unsplash.com/photo-1597805539244-3f0848b99ad7?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MjN8fG5lZ2VyaSUyMHNlbWJpbGFufGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
			],

			[
				'nama' => 'Port Dickson',
				'gambar' => 'https://images.unsplash.com/photo-1582035273990-90120cbaa23b?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mjd8fG5lZ2VyaSUyMHNlbWJpbGFufGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
			]
			
		]; */
	
		/* return view('homepage', ['all_pekan' => $all_pekan ]); */
		$db = db_connect();
		$result = $db->query('SELECT * FROM gambar');
		$all_pekan = $result->getResult();

		//dd($all_pekan);

		return view('homepage', ['all_pekan' => $all_pekan]);
	}

	function hello(){
		echo "<h1>Hello...</h1>";
	}

	function welcome() {
		echo "<h1>Welcome.....</h1>";
	}
}
